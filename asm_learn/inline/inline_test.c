#include<stdio.h>
int main() {

    int a=10, b;
    asm ("movl %1, %%eax\n\t" 
         "movl %%eax, %0\n\t"
         :"=r"(b)        /* output */
         :"r"(a)         /* input */
         :"%eax"         /* clobbered register */
         ); 
    printf("%d\n", b); 
    return 0;
}
