#include<stdio.h>


int main() {
    int my_var = 0;
    __asm__ __volatile__( "incl %0\n\t"
                            : "=m"(my_var)
                            : "m"(my_var)
                            : "memory");
    printf("%d\n", my_var);

}
